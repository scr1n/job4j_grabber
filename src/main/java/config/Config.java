package config;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class Config {
    private static Config instance;
    private final Properties properties = new Properties();


    public static Config getInstance() throws IOException {
        if (instance == null) {
            instance = new Config();
        }
        return instance;
    }

    private Config() throws IOException {
        properties.load(new FileInputStream("src/main/resources/rabbit.properties"));
    }

    public String getProperty(String property, String defaultValue) {
        return properties.getProperty(property, defaultValue);
    }


    public String getProperty(String property) {
        return properties.getProperty(property);
    }

}
