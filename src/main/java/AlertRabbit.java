import config.Config;
import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.quartz.JobBuilder.newJob;
import static org.quartz.SimpleScheduleBuilder.simpleSchedule;
import static org.quartz.TriggerBuilder.newTrigger;

public class AlertRabbit {
    public static void main(String[] args) throws IOException, SQLException {
        Config config = Config.getInstance();
        String rabbitInterval = config
                .getProperty("rabbit.interval", "10");


        try (Connection c = getConnection(config)) {
            schedule(rabbitInterval, c);
        }

    }

    private static void schedule(String rabbitInterval, Connection c) {
        try {
            List<Long> store = new ArrayList<>();
            Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();
            scheduler.start();
            JobDataMap data = new JobDataMap();
            data.put("store", store);
            data.put("connectionDb", c);
            JobDetail job = newJob(Rabbit.class)
                    .usingJobData(data)
                    .build();
            SimpleScheduleBuilder times = simpleSchedule()
                    .withIntervalInSeconds(Integer.parseInt(rabbitInterval))
                    .repeatForever();
            Trigger trigger = newTrigger()
                    .startNow()
                    .withSchedule(times)
                    .build();
            scheduler.scheduleJob(job, trigger);
            Thread.sleep(10000);
            scheduler.shutdown();
            System.out.println(store);
        } catch (Exception se) {
            se.printStackTrace();
        }
    }

    private static Connection getConnection(Config config) throws SQLException {
        return DriverManager
                .getConnection(
                        "jdbc:postgresql://" +
                                config.getProperty("db.host") + ":" +
                                config.getProperty("db.port") + "/" +
                                config.getProperty("db.name"),
                        config.getProperty("db.username"),
                        config.getProperty("db.password")
                );
    }

    public static class Rabbit implements Job {
        public Rabbit() {
            System.out.println("Хэш код " + hashCode());
        }

        @Override
        public void execute(JobExecutionContext context) {
            System.out.println("Rabbit runs here ...");
            JobDataMap jobDataMap = context.getJobDetail().getJobDataMap();
            List<Long> store = (List<Long>) jobDataMap.get("store");
            Connection connection = (Connection) jobDataMap.get("connectionDb");
            try {
                Statement statement = connection.createStatement();
                statement.execute("INSERT INTO rabbit " + "VALUES ('" +
                        new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())
                        + "')"
                );
            } catch (SQLException e) {
                e.printStackTrace();
            }
            store.add(System.currentTimeMillis());
            System.out.println( new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
        }
    }
}
